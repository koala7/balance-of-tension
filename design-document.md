# Balance of Tension
Made in the context of Postcard wargame design jam: https://itch.io/jam/postcards-from-the-front

## Constraints
- Games must fit on a 5" x 7" postcard (or metric equivalent) , both front and back can be used.
- Dice, 52 card decks, Coins may be part of the design and not included on the postcard.  
- Counters, Cubes, Player markers MUST be included on the postcard. (The spirit of a postcard game is that a postcard game is complete and self reliant, sans dice or 52 card decks or coins)

## Game

Ziel des Spiels: Möglichst viele Vorteile für sich herausspielen, ohne das der Gegner sein Gesicht verliert und den roten Knopf drückt.

- two player game
    - Sovjets (represented by Krushchov)
    - USA (represented by Kennedy)
- two meters
    - each nation has a secret _level of tension_ (tenseness?)
        - tension over a certain threshold → player presses red button → both loose
    - each nation has an open _perceived strength_
        - determines victory in the end
            - haben beide gleich viel punkte am ende → unentschieden
            - hat eine seite "leichtes" übergewicht → Seite gewinnt
            - hat eine Seite viel Übergewicht → Atomkrieg → beide verlieren
- the game board should actually contain a red button that the player who triggers the atomic war has to press → both loose
            
## Open questions
- should we (randomly) prime the tenseness meters at the beginning?


## Notes from first playtest
- / maybe switch "keep tracks secret" and "if either players tension track" sentences
- / need higher numbers. tensions didnÄt get near 15
- / turn  marker ugly
- / vertically mirror tracks for soviets
- / add "say 'ßEnoguh!'



## playtest mit roksana
- / rand um track marker, damit sie als marker erkennbar sind
- deescalate umdrehen? probably Not
- / many lives are lost
- tenion 15 already end of game or still fine


1. Submit a .pdf of your game. It can be two separate .pdfs, a front and a back, or one .pdf with both the front and back. 

2. Tell us a little about yourself and anything you wish to share about your design.

3. Tee Shirt Size

4. Graphic Makeover Yes or Noß
